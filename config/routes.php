<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Cake\Core\Configure;
use Symfony\Component\Yaml\Yaml;

$routerCallback = function (RouteBuilder $routes) {
    if (file_exists(CONFIG . 'infotechnohelp.json-api.yml')) {
        foreach (Yaml::parse(file_get_contents(CONFIG . 'infotechnohelp.json-api.yml'))['pathPrefixes'] ?? [] as $index => $pathPrefix) {
            $routes->prefix($pathPrefix, function (RouteBuilder $routes) {
                $routes->fallbacks(DashedRoute::class);
            });
        }
    }
};

Router::scope('/', $routerCallback);

$routerCallbacks = Configure::read('Infotechnohelp.Routes.Callbacks');

if ($routerCallbacks !== null) {
    Configure::write('Infotechnohelp.Routes.Callbacks', array_merge($routerCallbacks, ['json-api' => $routerCallback]));
}