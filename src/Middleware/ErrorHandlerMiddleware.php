<?php

namespace Infotechnohelp\JsonApi\Middleware;

use Infotechnohelp\JsonApi\Traits\RouteParser;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ErrorHandlerMiddleware
 * @package Infotechnohelp\JsonApi\Middleware
 */
class ErrorHandlerMiddleware extends \Cake\Error\Middleware\ErrorHandlerMiddleware
{
    use RouteParser;

    private function routeContainsApiPathPrefix($Request)
    {
        if (file_exists(CONFIG . 'infotechnohelp.json-api.yml')) {
            foreach (Yaml::parse(file_get_contents(CONFIG . 'infotechnohelp.json-api.yml'))['pathPrefixes'] ?? [] as $prefix) {
                if ($this->routeContains($Request, $prefix)) {
                    return true;
                }
            }
        }

        return false;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next)
    {

        if ($this->routeContainsApiPathPrefix($request)) {
            $this->exceptionRenderer = 'Infotechnohelp\JsonApi\Error\ApiExceptionRenderer';
        }

        return parent::__invoke($request, $response, $next);
    }
}
