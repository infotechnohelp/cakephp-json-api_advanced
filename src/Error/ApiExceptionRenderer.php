<?php

namespace Infotechnohelp\JsonApi\Error;

use Cake\Error\ExceptionRenderer;

/**
 * Class ApiExceptionRenderer
 * @package Infotechnohelp\JsonApi\Error
 */
class ApiExceptionRenderer extends ExceptionRenderer
{
    /**
     * @return \Cake\Http\Response
     */
    public function render()
    {
        $this->controller->response = $this->controller->response->withType('json');

        $responseErrorCode = 500;
        $actualErrorCode = $this->error->getCode();

        /** @link https://en.wikipedia.org/wiki/List_of_HTTP_status_codes */
        if (is_int($actualErrorCode) && $actualErrorCode >= 100 && $actualErrorCode <= 505) {
            $responseErrorCode = $actualErrorCode;
        }

        $this->controller->response = $this->controller->response->withStatus($responseErrorCode);

        return $this->controller->response->withStringBody(json_encode([
            'data' => null,
            'code' => $actualErrorCode,
            'message' => $this->error->getMessage(),
        ], JSON_PRETTY_PRINT));
    }
}
