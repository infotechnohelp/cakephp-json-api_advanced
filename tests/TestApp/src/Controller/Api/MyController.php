<?php

namespace TestApp\Controller\Api;

use Infotechnohelp\JsonApi\Traits\ApiController;
use Cake\Event\Event;
use Cake\Http\Exception\InternalErrorException;
use TestApp\Controller\AppController;

class MyController extends AppController
{
    use ApiController;

    public function getOne()
    {
        $this->_setResponse(1);
    }

    public function exception()
    {
        throw new InternalErrorException('Exception message');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->loadComponent('RequestHandler');
    }
}