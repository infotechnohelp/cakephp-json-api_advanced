<?php

namespace TestApp\Controller\Welcome;

use Infotechnohelp\JsonApi\Traits\ApiController;
use TestApp\Controller\AppController;

class MyController extends AppController
{
    use ApiController;

    public function getOne()
    {
        $this->_setResponse(1);
    }
}