<?php

namespace Infotechnohelp\JsonApi\Test\TestCase\Controller\Api;

use Cake\Core\Configure;
use Cake\TestSuite\IntegrationTestCase;
use Symfony\Component\Yaml\Yaml;

/**
 * Class MyControllerTest
 * @package Infotechnohelp\JsonApi\Test\TestCase\Controller\Api
 */
class MyControllerTest extends IntegrationTestCase
{
    public function testConfigMultiplePathPrefixes()
    {
        $this->get('api/my/get-one');
        $this->assertResponseOk();

        $this->get('welcome/my/get-one');
        $this->assertResponseOk();
    }

    public function testMissingPathPrefixesInConfig()
    {
        $this->get('any/my/get-one');

        $this->assertResponseCode(404);

        $this->assertContains('Error: Missing Controller', $this->_getBodyAsString());
    }

    public function testGetById()
    {
        $this->get('api/my/get-one');
        $this->assertResponseOk();
        $responseJson = $this->_getBodyAsString();
        $this->assertEquals(1, json_decode($responseJson, true)['data']);
    }

    public function testException()
    {
        $this->get('api/my/exception');
        $this->assertResponseCode(500);
        $responseJson = $this->_getBodyAsString();
        $this->assertEquals('Exception message', json_decode($responseJson, true)['message']);
    }

}
